Source: libolecf
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Hilko Bengen <bengen@debian.org>
Build-Depends: debhelper (>= 10~),
 pkg-config, python-dev, python3-dev, libbfio-dev, libfuse-dev,
Standards-Version: 4.1.1
Section: libs
Homepage: https://github.com/libyal/libolecf
Vcs-Git: https://salsa.debian.org/pkg-security-team/libolecf.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/libolecf

Package: libolecf-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
 libolecf1 (= ${binary:Version})
Description: OLE2 Compound File format access library -- development files
 libolecf is a library to access the OLE 2 Compound File (OLECF) format.
 .
 This package includes the development support files.

Package: libolecf1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends},
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: OLE2 Compound File format access library
 libolecf is a library to access the OLE 2 Compound File (OLECF) format.
 .
 This package contains the shared library.

Package: libolecf-utils
Section: otherosfs
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends},
 libolecf1,
Description: OLE2 Compound File format access library -- Utilities
 libolecf is a library to access the OLE 2 Compound File (OLECF) format.
 .
 This package contains tools to access data stored in OLECF files:
 olecfexport, olecfinfo, lecfmount.

Package: python-libolecf
Section: python
Architecture: any
Depends: libolecf1 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${python:Depends}
Description: OLE2 Compound File format access library -- Python 2 bindings
 libolecf is a library to access the OLE 2 Compound File (OLECF) format.
 .
 This package contains Python 2 bindings for libolecf.

Package: python3-libolecf
Section: python
Architecture: any
Depends: libolecf1 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: OLE2 Compound File format access library -- Python 3 bindings
 libolecf is a library to access the OLE 2 Compound File (OLECF) format.
 .
 This package contains Python 3 bindings for libolecf.
